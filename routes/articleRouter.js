/**
 * @Author: Hưng <hungls>
 * @Date:   2019-11-03T23:25:27+07:00
 * @Email:  hunglsxx@gmail.com
 * @Last modified by:   Hưng
 * @Last modified time: 2019-11-03T10:43:31+07:00
 */

var express = require('express');
const articleRouter = express.Router();
var helper = require('../helps');
var Article = require('../models/articleModel');
var db = firebase.firestore();
var ArticleCollection = db.collection('article');

articleRouter.route('/')
    .get((req, res) => {
        ArticleCollection
        .orderBy('created_time', 'desc')
        .limit(5)
        .get()
        .then(snapshot => {
            data = [];
            for (i in snapshot.docs) {
                var doc = snapshot.docs[i].data();
                doc.id = snapshot.docs[i].id;
                data.push(doc);
            }
            return res.json(data);
        })
        .catch(err => {
            return res.status(400).send(err);
        });
    })
    .post((req, res) => {
        try {
            var doc = helper.cleanJson(req.body);
            doc.created_time = helper.getUnixTimestampNow();
            doc.updated_time = helper.getUnixTimestampNow();

            var invalids = Article.validate(doc);
            if(invalids.length == 0) {
                ArticleCollection.add(doc).then(ref => {
                    doc.id = ref.id;
                    res.json(doc);
                }).catch((e) => {
                    return res.status(400).send(e);
                });
            } else {
                return res.status(400).send(invalids[0].message);
            }
        } catch (error) {
            return res.status(400).send(error);
        }
    });

articleRouter.use('/:docId', (req, res, next) => {
    req.doc = ArticleCollection.doc(req.params.docId);
    next();
});

articleRouter.route('/:docId')
    .get((req, res) => {
        req.doc.get()
        .then(doc => {
            if (!doc.exists) {
                return res.status(500).send('No such document!');
            } else {
                res.json(doc.data());
            }
        })
        .catch(err => {
            return res.status(400).send(err);
        });
    })
    .patch((req, res) => {
        var updateData = helper.cleanJson(req.body);
        updateData.created_time = helper.getUnixTimestampNow();
        
        var invalids = Article.validate(updateData);
        var va = Object.keys(updateData);
        var fi = [];
        for(i in invalids) {
            fi.push(invalids[i].path);
        }
        const scenario = fi.some(r=> va.indexOf(r) >= 0);
        if(invalids.length == 0 || !scenario) {
            req.doc.update(updateData).then(d => {
                return res.status(201).send("Updated "+req.params.docId);
            }).catch(e => {
                return res.status(400).send(e);
            });
        } else {
            return res.status(400).send(invalids[0].path + ' ' + invalids[0].message);
        }
    })
    .delete((req, res) => {
        let FieldValue = require('firebase-admin').firestore.FieldValue;

        var fields = Object.keys(Article.props);
        var deleteField = {};
        for (i in fields) {
            deleteField[fields[i]] = FieldValue.delete();
        }

        var batch = db.batch();
        batch.update(req.doc, deleteField);
        batch.delete(req.doc);

        batch.commit().then(function () {
            return res.status(201).send("Deleted "+req.params.docId);
        }).catch(function(e) {
            return res.status(400).send(e);
        });
    });

module.exports = articleRouter;
