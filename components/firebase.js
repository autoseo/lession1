/**
 * @Author: Hưng <hungls>
 * @Date:   2019-11-03T00:38:26+07:00
 * @Email:  hunglsxx@gmail.com
 * @Last modified by:   hungls
 * @Last modified time: 2019-11-03T11:06:07+07:00
 */

var admin = require("firebase-admin");

var serviceAccount = require("../firebase-key.json");
var config = require('../config');

module.exports = {
    admin: function() {
        if(typeof global.firebase === 'undefined') {
            global.firebase = admin.initializeApp({
                credential: admin.credential.cert(serviceAccount),
                databaseURL: config.firebase.databaseURL
            },'init_admin');
        }
        return global.firebase;
    }
};
