
var Schema = require('validate');

const article = new Schema({
    title: {type: String, required: true},
    description: {type: String},
    content: {type: String, required: true},
    
    created_time: {type: Number},
    updated_time: {type: Number}
});

module.exports = article;