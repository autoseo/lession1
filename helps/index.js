/**
 * @Author: Hưng <hungls>
 * @Date:   2019-07-10T00:38:26+07:00
 * @Email:  hunglsxx@gmail.com
 * @Last modified by:   hungls
 * @Last modified time: 2019-07-10T11:06:07+07:00
 */

module.exports = {
    getUnixTimestampNow: function() {
        return Math.round(new Date().getTime()/1000);
    },
    strtotime: function(str) {
        return Math.round((Date.parse(str))/1000);
    },
    vnToString: function(str) {
        str = str.replace(/á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ/g, 'a');
        str = str.replace(/đ|Đ/g, 'd');
        str = str.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ/g, 'e');
        str = str.replace(/í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị/g, 'i');
        str = str.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ/g, 'o');
        str = str.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự/g, 'u');
        str = str.replace(/ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ/g, 'y');
        str = str.replace(/\s+/g, '-');
        str = str.replace(/[^\w+]/g, '-');
        str = str.replace(/_+/g, '-');
        return str;
    },
    cleanJson: function(obj) {
        if(typeof obj == 'object') {
            obj = JSON.stringify(obj);
            obj = JSON.parse(obj);
            return obj;
        }
        return {};
    }
};
