
DIRECTORY STRUCTURE
-------------------

      bin/               contains build scripts
      components/        contains components eg firebase, mongo, ...
      helps/             contains application helper
      middlewares/       contains middleware classes
      models/            contains model classes
      public/            contains files generated during runtime
      routes/            contains all routes (controllers)
      views/             contains view files for the Web application



REQUIREMENTS
------------

The minimum requirement by this project that your nodejs >= 8.x.

CONFIGURATION
-------------

### cfg

Edit (or create) the file `config.js` with real data, for example:

```js 
var config = {};
config.firebase = {};
config.firebase.databaseURL = 'https://your-project.firebaseio.com';
module.exports = config;

```

### firebase-key.js
Generate firebase key 

INSTALLATION
------------

### Step 1: install all pakages used in project via npm

You can then install this project using the following command:

~~~
npm install
~~~

### Step 2: 
~~~
npm start
~~~


TESTING
-------
~~~
POSTMAN
~~~