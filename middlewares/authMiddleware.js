/**
 * @Author: Hưng
 * @Date:   2019-07-31T15:36:54+07:00
 * @Email:  hunglsxx@gmail.com
 * @Last modified by:   Hưng
 * @Last modified time: 2019-07-31T15:37:50+07:00
 */

var config = require('../config');

module.exports = function (options) {
    return async function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.setHeader('Access-Control-Expose-Headers', 'Access-Control-*, Origin, X-Requested-With, Content-Type, Accept, Authorization');
        res.setHeader('Access-Control-Allow-Methods', 'HEAD, GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Access-Control-*, Origin, X-Requested-With, Content-Type, Accept, Authorization');
        res.setHeader('Access-Control-Allow-Credentials', true);

        const token = req.header('AUTH_TOKEN');
        if (typeof config.auth.tokens[token] !== 'undefined' 
        && !config.auth.tokens[token].is_expired) {
            req.project = config.auth.tokens[token];
        } else {
            return res.status(500).send('AUTH_TOKEN is expired or undefined');
        }
        next();
    }
}
